$(document).ready(function(){
    ['paste'].forEach(function(event) {
        document.addEventListener(event, function(e) {
            e.preventDefault();
            var pastedText = undefined;

            if (window.clipboardData && window.clipboardData.getData) { // IE
                pastedText = window.clipboardData.getData('Text');
            } else if (e.clipboardData && e.clipboardData.getData) {
                pastedText = e.clipboardData.getData('text/html');
            }
	    	
	    	var sourceText = $("#output").text(pastedText).val();
			var cleanText = sourceText.match(/(<table[^>]*>(?:.|\n)*?<\/table>)/g);

			var walk_the_DOM = function walk(node, func) {
			    func(node);
			    node = node.firstChild;
			    while (node) {
			        walk(node, func);
			        node = node.nextSibling;
			    }
			};

			var result = "";
			var wrapper= document.createElement('div');
			
			for (i = 0; i < cleanText.length; i++) {
				wrapper.innerHTML= cleanText[i];
				walk_the_DOM(wrapper.firstChild, function(element) {
				    if(element.removeAttribute) {
				        element.removeAttribute('id');
				        element.removeAttribute('style');
				        element.removeAttribute('class');
				        element.removeAttribute('width');
				        element.removeAttribute('size');
				    }
				});
				result += wrapper.innerHTML;
				result += '<br>';
			}
			
			// Clean extra tags
			var expr = /<\/?span>|<\/?o:p>|<\/?font>/;
			while (expr.test(result)) {
				result = result.replace(expr,'');
			}				

			// Check table border
			expr = /<table cell/;
			while (expr.test(result)) {
				result = result.replace(/<table cell/,'<table border="1" cell');
			}	

			$("#output").text(result);
	        $("#target").html(result);
        });
    });
});